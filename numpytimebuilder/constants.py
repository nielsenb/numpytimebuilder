# -*- coding: utf-8 -*-

# Copyright (c) 2025, Brandon Nielsen
# All rights reserved.
#
# This software may be modified and distributed under the terms
# of the BSD license.  See the LICENSE file for details.

from numpytimebuilder import compat

MONTHS_PER_YEAR = compat.long(12)
DAYS_PER_YEAR = compat.long(365)
DAYS_PER_MONTH = compat.long(30)
DAYS_PER_WEEK = compat.long(7)
HOURS_PER_DAY = compat.long(24)
MINUTES_PER_HOUR = compat.long(60)
SECONDS_PER_MINUTE = compat.long(60)
MILLISECONDS_PER_SECOND = compat.long(1000)
MICROSECONDS_PER_MILLISECOND = compat.long(1000)
NANOSECONDS_PER_MICROSECOND = compat.long(1000)
PICOSECONDS_PER_NANOSECOND = compat.long(1000)
FEMTOSECONDS_PER_PICOSECOND = compat.long(1000)
ATTOSECONDS_PER_FEMTOSECOND = compat.long(1000)
